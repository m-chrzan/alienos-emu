CC=gcc
CFLAGS=-Wall -std=c99

emu: emu.o screen.o rand.o alienos.o elfhandler.o
	$(CC) $(CFLAGS) -lncurses -o emu emu.o screen.o rand.o alienos.o elfhandler.o

screen.o: screen.c screen.h
	$(CC) $(CFLAGS) -c -o screen.o screen.c

rand.o: rand.c rand.h
	$(CC) $(CFLAGS) -c -o rand.o rand.c

emu.o: emu.c
	$(CC) $(CFLAGS) -c -o emu.o emu.c

alienos.o: alienos.c alienos.h
	$(CC) $(CFLAGS) -c -o alienos.o alienos.c

elfhandler.o: elfhandler.c elfhandler.h
	$(CC) $(CFLAGS) -c -o elfhandler.o elfhandler.c

.PHONY: clean

clean:
	rm -rf *.o emu
