#ifndef ALIENOS_H
#define ALIENOS_H

#include <elf.h>

#define IS_END_CODE(x) ((x) & 0x80)
#define MAKE_END_CODE(x) ((x) | 0x80)
#define GET_END_CODE(x) ((x) & 0x7f)

int init_alienos(pid_t pid);
int cleanup_alienos();
int set_params(pid_t pid, int argc, char *argv[], Elf64_Phdr *params_header);

int do_syscall(struct user_regs_struct *regs, pid_t pid);

#endif
