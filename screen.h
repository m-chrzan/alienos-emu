#ifndef SCREEN_H
#define SCREEN_H

#include <stdint.h>

#define SK_ENTER 0x0a
#define SK_UP 0x80
#define SK_LEFT 0x81
#define SK_DOWN 0x82
#define SK_RIGHT 0x83
#define SK_ISPRINTABLE(ch) ((ch) >= 0x20 && (ch) <= 0x7e)
#define SK_NOKEY -1

#define SC_BLACK 0
#define SC_BLUE 1
#define SC_GREEN 2
#define SC_TURQUOISE 3
#define SC_RED 4
#define SC_PINK 5
#define SC_YELLOW 6
#define SC_LIGHTGREY 7
#define SC_DARKGREY 8
#define SC_LIGHTBLUE 9
#define SC_LIGHTGREEN 10
#define SC_LIGHTTURQUOISE 11
#define SC_LIGHTRED 12
#define SC_LIGHTPINK 13
#define SC_LIGHTYELLOW 14
#define SC_WHITE 15

#define SC_ISLIGHT(c) ((c) == SC_LIGHTGREY || (c) >= SC_LIGHTBLUE)

#define SC_GETCHAR(x) ((x) & 0xff)
#define SC_GETCOLOR(x) (((x) >> 8) & 0xf)

int init_screen();
void cleanup_screen();

int getkey();
int print(int x, int y, uint16_t *chars, int n);
int setcursor(int x, int y);

#endif
