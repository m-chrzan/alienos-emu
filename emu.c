#define _POSIX_C_SOURCE 200809
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <sys/prctl.h>
#include <sys/ptrace.h>
#include <asm/ptrace.h>
#include <sys/types.h>
#include <sys/user.h>
#include <sys/wait.h>
#include <unistd.h>

#include "alienos.h"
#include "elfhandler.h"

#define EMU_ERR 127

void run_prog(char *prog_filename) {
    int rv = prctl(PR_SET_PDEATHSIG, SIGHUP);
    if (rv == -1) {
        exit(1);
    }

    rv = ptrace(PTRACE_TRACEME, 0, NULL, NULL);
    if (rv == -1) {
        exit(1);
    }

    char *args = NULL;

    rv = execve(prog_filename, &args, NULL);
    if (rv == -1) {
        exit(1);
    }
}

int emulate(pid_t pid, char *prog_filename, int argc, char *argv[]) {
    int ret;
    int status;
    struct user_regs_struct regs;

    int rv = waitpid(pid, &status, 0);
    if (rv == -1 || !WIFSTOPPED(status) || WSTOPSIG(status) != SIGTRAP) {
        return EMU_ERR;
    };

    rv = ptrace(PTRACE_SETOPTIONS, pid, NULL, PTRACE_O_EXITKILL);
    if (rv == -1) {
        return EMU_ERR;
    }

    if (!init_alienos(pid)) {
        return EMU_ERR;
    }

    Elf64_Phdr params_header;
    rv = get_params_header(prog_filename, &params_header);
    if (!rv) {
        goto error;
    }

    if (rv == FOUND) {
        if (!set_params(pid, argc, argv, &params_header)) {
            goto error;
        }
    } else {
        if (argc != 0) {
            goto error;
        }
    }

    rv = ptrace(PTRACE_SYSEMU, pid, NULL, NULL);
    if (rv == -1) {
        goto error;
    }

    for (;;) {
        rv = waitpid(pid, &status, 0);
        if (rv == -1 || !WIFSTOPPED(status) || WSTOPSIG(status) != SIGTRAP) {
            goto error;
        };

        rv = ptrace(PTRACE_GETREGS, pid, NULL, &regs);
        if (rv == -1) {
            goto error;
        }

        rv = do_syscall(&regs, pid);
        if (!rv) {
            goto error;
        } else if (IS_END_CODE(rv)) {
            ret = GET_END_CODE(rv);
            goto cleanup;
        }

        rv = ptrace(PTRACE_SETREGS, pid, NULL, &regs);
        if (rv == -1) {
            goto error;
        }

        rv = ptrace(PTRACE_SYSEMU, pid, NULL, NULL);
        if (rv == -1) {
            goto error;
        }
    }

error:
    ret = EMU_ERR;
cleanup:
    cleanup_alienos();

    return ret;
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        return EMU_ERR;
    }

    char *prog_filename = argv[1];

    pid_t pid = fork();
    switch (pid) {
        case -1:
            return EMU_ERR;
        case 0:
            run_prog(prog_filename);
            return 0;
        default:
            return emulate(pid, prog_filename, argc - 2, argv + 2);
    }
}
