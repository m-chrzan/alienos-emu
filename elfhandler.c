#define _POSIX_C_SOURCE 200809L
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "elfhandler.h"

#define PT_PARAMS 0x60031337

int get_params_header(char *prog_filename, Elf64_Phdr *params_header) {
    int fd = open(prog_filename, O_RDONLY);
    if (fd == -1) {
        return 0;
    }

    Elf64_Ehdr header;
    ssize_t rv = pread(fd, &header, sizeof(Elf64_Ehdr), 0);
    if (rv != sizeof(Elf64_Ehdr)) {
        return 0;
    }

    for (int i = 0; i < header.e_phnum; i++) {
        rv = pread(fd, params_header, sizeof(Elf64_Phdr),
                header.e_phoff + i * header.e_phentsize);
        if (rv != sizeof(Elf64_Phdr)) {
            return 0;
        }

        if (params_header->p_type == PT_PARAMS) {
            return FOUND;
        }
    }

    return NOT_FOUND;
}
