#ifndef ELFHANDLER_H
#define ELFHANDLER_H

#include <elf.h>

#define FOUND 1
#define NOT_FOUND 2

int get_params_header(char *prog_filename, Elf64_Phdr *params_header);

#endif
