#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>

#include "rand.h"

int randfd;

int init_rand() {
    randfd = open("/dev/urandom", O_RDONLY);
    if (randfd == -1) {
        return 0;
    }

    return 1;
}

void cleanup_rand() {
    close(randfd);
}

int getrand(uint32_t *dest) {
    ssize_t rv = read(randfd, dest, 4);
    if (rv < 4) {
        return 0;
    }

    return 1;
}
