#ifndef RAND_H
#define RAND_H
#include <stdint.h>

int init_rand();
void cleanup_rand();

int getrand(uint32_t *dest);

#endif
