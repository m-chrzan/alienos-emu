#include <curses.h>
#include <locale.h>

#include "screen.h"

int init_colors() {
    int err = start_color();
    if (err == ERR) {
        return 0;
    }

    init_pair(SC_BLACK, COLOR_BLACK, COLOR_BLACK);
    init_pair(SC_BLUE, COLOR_BLUE, COLOR_BLACK);
    init_pair(SC_GREEN, COLOR_GREEN, COLOR_BLACK);
    init_pair(SC_TURQUOISE, COLOR_CYAN, COLOR_BLACK);
    init_pair(SC_RED, COLOR_RED, COLOR_BLACK);
    init_pair(SC_PINK, COLOR_MAGENTA, COLOR_BLACK);
    init_pair(SC_YELLOW, COLOR_YELLOW, COLOR_BLACK);
    init_pair(SC_LIGHTGREY, COLOR_WHITE, COLOR_BLACK);
    init_pair(SC_DARKGREY, COLOR_BLACK, COLOR_BLACK);
    init_pair(SC_LIGHTBLUE, COLOR_BLUE, COLOR_BLACK);
    init_pair(SC_LIGHTGREEN, COLOR_GREEN, COLOR_BLACK);
    init_pair(SC_LIGHTTURQUOISE, COLOR_CYAN, COLOR_BLACK);
    init_pair(SC_LIGHTRED, COLOR_RED, COLOR_BLACK);
    init_pair(SC_LIGHTPINK, COLOR_MAGENTA, COLOR_BLACK);
    init_pair(SC_LIGHTYELLOW, COLOR_YELLOW, COLOR_BLACK);
    init_pair(SC_WHITE, COLOR_WHITE, COLOR_BLACK);

    return 1;
}

int init_screen() {
    char *rv = setlocale(LC_ALL, "");
    if (rv == NULL) {
        return 0;
    }

    WINDOW *screen = initscr();
    if (screen == NULL) {
        return 0;
    }

    int err = cbreak();
    if (err == ERR) {
        goto error;
    }

    if (!init_colors()) {
        goto error;
    }

    err = noecho();
    if (err == ERR) {
        goto error;
    }

    err = nl();
    if (err == ERR) {
        goto error;
    }

    err = intrflush(stdscr, TRUE);
    if (err == ERR) {
        goto error;
    }

    err = keypad(stdscr, TRUE);
    if (err == ERR) {
        goto error;
    }

    return 1;

error:
    cleanup_screen();
    return 0;
}

void cleanup_screen() {
    endwin();
}

int getkey() {
    int ch = getch();

    switch (ch) {
    case ERR:
        return SK_NOKEY;
    case '\n':
        return SK_ENTER;
    case KEY_UP:
        return SK_UP;
    case KEY_LEFT:
        return SK_LEFT;
    case KEY_DOWN:
        return SK_DOWN;
    case KEY_RIGHT:
        return SK_RIGHT;
    default:
        if (SK_ISPRINTABLE(ch)) {
            return ch;
        } else {
            return SK_NOKEY;
        }
    }
}

int print(int x, int y, uint16_t *chars, int n) {
    int current_x, current_y;
    int err;
    getyx(stdscr, current_y, current_x);

    for (int i = 0; i < n; i++) {
        int color = SC_GETCOLOR(chars[i]);
        if (SC_ISLIGHT(color)) {
            if (attron(A_BOLD) == ERR) {
                return 0;
            }
        }

        err = mvaddch(y, x+i, SC_GETCHAR(chars[i]) | COLOR_PAIR(color));

        if (err == ERR) {
            return 0;
        }

        if (SC_ISLIGHT(color)) {
            if (attroff(A_BOLD) == ERR) {
                return 0;
            }
        }
    }


    err = move(current_y, current_x);
    if (err == ERR) {
        return 0;
    }

    err = refresh();
    if (err == ERR) {
        return 0;
    }

    return 1;
}

int setcursor(int x, int y) {
    int err = move(y, x);
    if (err == ERR) {
        return 0;
    }

    err = refresh();
    if (err == ERR) {
        return 0;
    }

    return 1;
}
