#define _POSIX_C_SOURCE 200809L
#include <fcntl.h>
#include <sys/user.h>
#include <sys/types.h>
#include <sys/ptrace.h>
#include <sys/stat.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "alienos.h"

#include "rand.h"
#include "screen.h"

#define SYS_END 0
#define SYS_GETRAND 1
#define SYS_GETKEY 2
#define SYS_PRINT 3
#define SYS_SETCURSOR 4

int mem_fd;

int init_mem(pid_t pid) {
    char mem_filename[32];
    int rv = sprintf(mem_filename, "/proc/%d/mem", pid);
    if (rv < 0) {
        return 0;
    }

    mem_fd = open(mem_filename, O_RDWR);

    if (mem_fd == -1) {
        return 0;
    }

    return 1;
}

void cleanup_mem() {
    close(mem_fd);
}

int init_alienos(pid_t pid) {
    if (!init_rand()) {
        goto error_rand;
    }

    if (!init_screen()) {
        goto error_screen;
    }

    if (!init_mem(pid)) {
        goto error_mem;
    }

    return 1;

error_mem:
    cleanup_screen();
error_screen:
    cleanup_rand();
error_rand:

    return 0;
}

int parse_arg(char *src, int32_t *res) {
    if (src[0] == '\0') {
        return 0;
    }

    char *endptr;
    *res = strtol(src, &endptr, 10);

    if (*endptr != '\0') {
        return 0;
    }

    return 1;
}

int set_params(pid_t pid, int argc, char *argv[], Elf64_Phdr *params_header) {
    if (params_header->p_memsz / 4 != argc) {
        return 0;
    }

    for (int i = 0; i < argc; i++) {
        int32_t arg;
        if (!parse_arg(argv[i], &arg)) {
            return 0;
        }

        ssize_t rv = pwrite(mem_fd, &arg, 4, params_header->p_paddr + (i * 4));
        if (rv != 4) {
            return 0;
        }
    }

    return 1;
}

int cleanup_alienos() {
    cleanup_rand();
    cleanup_screen();
    cleanup_mem();
    return 0;
}

int do_end(struct user_regs_struct *regs) {
    uint64_t code = regs->rdi;

    if (code < 0 || code > 63) {
        return 0;
    }

    return MAKE_END_CODE(code);
}

int do_getrand(struct user_regs_struct *regs) {
    uint32_t rand;
    if (!getrand(&rand)) {
        return 0;
    }

    regs->rax = rand;

    return 1;
}

int do_getkey(struct user_regs_struct *regs) {
    int key = getkey();
    if (key == SK_NOKEY) {
        return 0;
    }

    regs->rax = key;

    return 1;
}

int do_print(struct user_regs_struct *regs, pid_t pid) {
    int x = (int)regs->rdi;
    int y = (int)regs->rsi;
    off_t start = (off_t)regs->rdx;
    int n = (int)regs->r10;

    uint16_t *chars = malloc(sizeof(uint16_t) * n);
    if (chars == NULL) {
        goto error_malloc;
    }

    ssize_t rv = pread(mem_fd, chars, sizeof(uint16_t) * n, start);
    if (rv != sizeof(uint16_t) * n) {
        goto error;
    }

    if (!print(x, y, chars, n)) {
        goto error;
    }

    free(chars);

    return 1;

error:
    free(chars);
error_malloc:

    return 0;
}

int do_setcursor(struct user_regs_struct *regs) {
    int x = (int)regs->rdi;
    int y = (int)regs->rsi;

    if (!setcursor(x, y)) {
        return 0;
    }

    return 1;
}

int do_syscall(struct user_regs_struct *regs, pid_t pid) {
    switch(regs->orig_rax) {
    case SYS_END:
        return do_end(regs);
    case SYS_GETRAND:
        return do_getrand(regs);
    case SYS_GETKEY:
        return do_getkey(regs);
    case SYS_PRINT:
        return do_print(regs, pid);
    case SYS_SETCURSOR:
        return do_setcursor(regs);
    default:
        return 0;
    }
}
